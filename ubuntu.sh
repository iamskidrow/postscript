#! /bin/bash

basedir=$home/Tools
baseinstall=sudo python3 setup.py install

sudo apt-get -y update
sudo apt-get -y upgrade

#Hacking and basic Stuff
sudo apt-get install -y libcurl4-openssl-dev libssl-dev jq ruby-full libcurl4-openssl-dev libxml2 libxml2-dev libxslt1-dev ruby-dev build-essential libgmp-dev zlib1g-dev build-essential libssl-dev libffi-dev python-dev python-setuptools libldns-dev python-dnspython git rename yarn gulp curl proxychains tor torbrowser-launcher mlocate ubuntu-restricted-extras net-tools nodejs npm ruby ruby-dev perl python python3 python3-pip pipenv git netcat recordmydesktop neofetch screenfetch gcc gpp mitmproxy sqlmap dirb dnsrecon apktool rkhunter pompem websploit phantomjs nikto nmap wireshark recon-ng john hashcat whois whatweb weevely gobuster openvpn exiftool knockpy
sudo snap install go

#Hacking stuff [SnapStore]
sudo snap install amass
sudo snap install chromium
sudo gem install wpscan

#Sublime text 3
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add - && sudo apt-get install apt-transport-https && echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list && sudo apt-get update -y && sudo apt-get install sublime-text -y 

#Google chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb; sudo dpkg -i google-chrome-stable_current_amd64.deb && rm google-chrome-stable_current_amd64.deb 

#VsCode
sudo snap install code --classic

#Making "Tools" Directory in $home
cd $home; mkdir Tools; cd Tools

#Cloning tools
git clone https://github.com/Dionach/CMSmap.git
git clone https://github.com/maurosoria/dirsearch.git
git clone https://github.com/aboul3la/Sublist3r.git
git clone https://github.com/blechschmidt/massdns.git
git clone https://github.com/danielmiessler/SecLists.git
git clone https://github.com/robertdavidgraham/masscan.git
git clone https://github.com/xmendez/wfuzz.git
git clone https://github.com/EnableSecurity/wafw00f.git
git clone https://github.com/nahamsec/JSParser.git
git clone https://github.com/guelfoweb/knock.git
git clone https://github.com/maaaaz/webscreenshot.git
git clone https://github.com/iamskidrow/autocon.git
go get -u github.com/tomnomnom/assetfinder
go get -u github.com/tomnomnom/meg
go get -u github.com/hakluke/hakrawler
go get -u github.com/tomnomnom/httprobe
go get -u github.com/tomnomnom/unfurl
go get -u github.com/tomnomnom/waybackurls

#Go tools installed in one shot
cd $home/go/bin; sudo chmod +x *; sudo mv * /usr/bin
#Cmsmap installation
cd $basedir/CMSmap; $baseinstall
#Sublist3r installation
cd $basedir/Sublist3r; $baseinstall
#Massdns installation
cd $basedir/massdns; make; cd bin; chmod +x massdns; mv massdns /usr/bin
#Masscan installation
cd $basedir/Masscan; make; cd bin; chmod +x masscan; mv masscan /usr/bin
#Wfuzz installation
cd $basedir/wfuzz; $baseinstall
#Wafw00f installation
cd $basedir/wafw00f; $baseinstall
#JSParser installation
cd $basedir/JSParser; $baseinstall
#KnockPy installation
cd $basedir/knock; $baseinstall
#autocon installation
cd $basedir/autocon; sudo chmod +x autocon.sh; sudo cp autocon.sh /usr/bin/autocon

#Kali tools
sudo apt install -y ace-voip;sudo apt install -y amap;sudo apt install -y arp-scan;sudo apt install -y bing-ip2hosts;sudo apt install -y braa;sudo apt install -y casefile;sudo apt install -y cdpsnarf;sudo apt install -y cisco-torch;sudo apt install -y copy-router-config;sudo apt install -y dmitry;sudo apt install -y dnsenum;sudo apt install -y dnsmap;sudo apt install -y dnsrecon;sudo apt install -y dnstracer;sudo apt install -y dnswalk;sudo apt install -y dotdotpwn;sudo apt install -y enum4linux;sudo apt install -y enumiax;sudo apt install -y eyewitness;sudo apt install -y faraday;sudo apt install -y fierce;sudo apt install -y firewalk;sudo apt install -y fragrouter;sudo apt install -y goofile;sudo apt install -y hping3;sudo apt install -y ident-user-enum;sudo apt install -y inspy;sudo apt install -y intrace;sudo apt install -y ismtp;sudo apt install -y lbd;sudo apt install -y maltego;sudo apt install -y masscan;sudo apt install -y metagoofil;sudo apt install -y nbtscan-unixwiz;sudo apt install -y nikto;sudo apt install -y nmap;sudo apt install -y osrframework;sudo apt install -y p0f;sudo apt install -y parsero;sudo apt install -y recon-ng;sudo apt install -y set;sudo apt install -y smbmap;sudo apt install -y smtp-user-enum;sudo apt install -y sslsplit;sudo apt install -y sslyze;sudo apt install -y sublist3r;sudo apt install -y thc-ipv6;sudo apt install -y theharvester;sudo apt install -y tlssled;sudo apt install -y twofi;sudo apt install -y unicornscan;sudo apt install -y urlcrazy;sudo apt install -y wireshark;sudo apt install -y xplico;sudo apt install -y bed;sudo apt install -y cisco-auditing-tool;sudo apt install -y cisco-global-exploiter;sudo apt install -y cisco-ocs;sudo apt install -y cisco-torch;sudo apt install -y copy-router-config;sudo apt install -y doona;sudo apt install -y dotdotpwn;sudo apt install -y jsql;sudo apt install -y lynis;sudo apt install -y nmap;sudo apt install -y ohrwurm;sudo apt install -y openvas;sudo apt install -y oscanner;sudo apt install -y sfuzz;sudo apt install -y sidguesser;sudo apt install -y siparmyknife;sudo apt install -y sqlmap;sudo apt install -y sqlninja;sudo apt install -y sqlsus;sudo apt install -y thc-ipv6;sudo apt install -y tnscmd10g;sudo apt install -y unix-privesc-check;sudo apt install -y yersinia;sudo apt install -y armitage;sudo apt install -y beef;sudo apt install -y cisco-auditing-tool;sudo apt install -y cisco-global-exploiter;sudo apt install -y cisco-ocs;sudo apt install -y cisco-torch;sudo apt install -y commix;sudo apt install -y crackle;sudo apt install -y exploitdb;sudo apt install -y jboss-autopwn;sudo apt install -y maltego;sudo apt install -y metasploit-framework;sudo apt install -y msfpc;sudo apt install -y routersploit;sudo apt install -y set;sudo apt install -y shellnoob;sudo apt install -y sqlmap;sudo apt install -y thc-ipv6;sudo apt install -y yersinia;sudo apt install -y asleap;sudo apt install -y bluelog;sudo apt install -y blueranger;sudo apt install -y bluesnarfer;sudo apt install -y bully;sudo apt install -y cowpatty;sudo apt install -y crackle;sudo apt install -y eapmd5pass;sudo apt install -y freeradius-wpe;sudo apt install -y hostapd-wpe;sudo apt install -y kalibrate-rtl;sudo apt install -y killerbee;sudo apt install -y kismet;sudo apt install -y mdk3;sudo apt install -y mfcuk;sudo apt install -y mfoc;sudo apt install -y mfterm;sudo apt install -y multimon-ng;sudo apt install -y pixiewps;sudo apt install -y reaver;sudo apt install -y redfang;sudo apt install -y spooftooph;sudo apt install -y wifiphisher;sudo apt install -y wifite;sudo apt install -y binwalk;sudo apt install -y bulk-extractor;sudo apt install -y chntpw;sudo apt install -y dc3dd;sudo apt install -y ddrescue;sudo apt install -y dumpzilla;sudo apt install -y extundelete;sudo apt install -y foremost;sudo apt install -y galleta;sudo apt install -y guymager;sudo apt install -y p0f;sudo apt install -y pdf-parser;sudo apt install -y pdfid;sudo apt install -y regripper;sudo apt install -y volatility;sudo apt install -y xplico;sudo apt install -y apache-users;sudo apt install -y arachni;sudo apt install -y cutycsudo apt;sudo apt install -y davtest;sudo apt install -y dirb;sudo apt install -y dirbuster;sudo apt install -y gobuster;sudo apt install -y hurl;sudo apt install -y jboss-autopwn;sudo apt install -y joomscan;sudo apt install -y jsql;sudo apt install -y maltego;sudo apt install -y nikto;sudo apt install -y padbuster;sudo apt install -y paros;sudo apt install -y parsero;sudo apt install -y plecost;sudo apt install -y recon-ng;sudo apt install -y skipfish;sudo apt install -y sqlmap;sudo apt install -y sqlninja;sudo apt install -y sqlsus;sudo apt install -y uniscan;sudo apt install -y webscarab;sudo apt install -y websploit;sudo apt install -y wfuzz;sudo apt install -y whatweb;sudo apt install -y wpscan;sudo apt install -y xsser;sudo apt install -y zaproxy;sudo apt install -y dhcpig;sudo apt install -y iaxflood;sudo apt install -y inviteflood;sudo apt install -y ipv6-toolkit;sudo apt install -y mdk3;sudo apt install -y reaver;sudo apt install -y rtpflood;sudo apt install -y slowhttptest;sudo apt install -y t50;sudo apt install -y termineter;sudo apt install -y thc-ipv6;sudo apt install -y thc-ssl-dos;sudo apt install -y bettercap;sudo apt install -y dnschef;sudo apt install -y fiked;sudo apt install -y hamster-sidejack;sudo apt install -y hexinject;sudo apt install -y iaxflood;sudo apt install -y inviteflood;sudo apt install -y ismtp;sudo apt install -y isr-evilgrade;sudo apt install -y mitmproxy;sudo apt install -y ohrwurm;sudo apt install -y protos-sip;sudo apt install -y rebind;sudo apt install -y responder;sudo apt install -y rtpbreak;sudo apt install -y rtpinsertsound;sudo apt install -y rtpmixsound;sudo apt install -y sctpscan;sudo apt install -y siparmyknife;sudo apt install -y sipp;sudo apt install -y sipvicious;sudo apt install -y sniffjoke;sudo apt install -y sslsplit;sudo apt install -y thc-ipv6;sudo apt install -y voiphopper;sudo apt install -y webscarab;sudo apt install -y wireshark;sudo apt install -y xspy;sudo apt install -y yersinia;sudo apt install -y zaproxy;sudo apt install -y brutespray;sudo apt install -y cewl;sudo apt install -y chntpw;sudo apt install -y cisco-auditing-tool;sudo apt install -y cmospwd;sudo apt install -y crowbar;sudo apt install -y crunch;sudo apt install -y gpp-decrypt;sudo apt install -y hash-identifier;sudo apt install -y hashcat;sudo apt install -y hydra;sudo apt install -y john;sudo apt install -y johnny;sudo apt install -y maltego;sudo apt install -y maskprocessor;sudo apt install -y multiforcer;sudo apt install -y ncrack;sudo apt install -y oclgausscrack;sudo apt install -y ophcrack;sudo apt install -y pack;sudo apt install -y patator;sudo apt install -y polenum;sudo apt install -y rainbowcrack;sudo apt install -y rcracki-mt;sudo apt install -y rsmangler;sudo apt install -y seclists;sudo apt install -y sqldict;sudo apt install -y statsprocessor;sudo apt install -y thc-pptp-bruter;sudo apt install -y truecrack;sudo apt install -y webscarab;sudo apt install -y wordlists;sudo apt install -y zaproxy;sudo apt install -y cryptcat;sudo apt install -y cymothoa;sudo apt install -y dbd;sudo apt install -y dns2tcp;sudo apt install -y httptunnel;sudo apt install -y nishang;sudo apt install -y polenum;sudo apt install -y powersploit;sudo apt install -y pwnat;sudo apt install -y ridenum;sudo apt install -y sbd;sudo apt install -y shellter;sudo apt install -y webshells;sudo apt install -y weevely;sudo apt install -y winexe;sudo apt install -y android-sdk;sudo apt install -y apktool;sudo apt install -y arduino;sudo apt install -y dex2jar;sudo apt install -y sakis3g;sudo apt install -y smali;

sudo apt-get full-upgrade -y
